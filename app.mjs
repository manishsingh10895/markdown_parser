import dotenv from 'dotenv';

dotenv.config();

const API_KEY="131a346523d94e9a87b92652232909"
const SECRET_API_KEY = process.env.API_KEY;

const PLACE = "London";

const BASE_URL = `https://api.weatherapi.com/v1/current.json?key=${SECRET_API_KEY}&q=${PLACE}&aqi=no`;

async function check() {
  try {
    const res = await fetch(BASE_URL);
    const data = await res.json();

    console.log(data);
  } catch (error) {
    console.error(error);
  }
}

check();
